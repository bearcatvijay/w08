(function ($) {

    $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
    $('#btnAjax').click(function () {
        // Perform an asynchronous HTTP (Ajax) API request.
        axios.get('https://jsonplaceholder.typicode.com/photos/1').then(function (response) {
            $('#showResult').html(response.data.title)
        })
    });
  
  })($);
  